import { ModuleWithProviders } from '../../../node_modules/@angular/core';
import { Routes, RouterModule, RouterOutlet, RouterLink, RouterLinkWithHref, RouterLinkActive }
    from '../../../node_modules/@angular/router';
import { AppComponent } from './app.component';
import { LoyaltyComponent } from './home/home.component';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '../../../node_modules/@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';

export const appRoutes: Routes = [
    {
        path: 'Loyalty',
        component: LoyaltyComponent
    }
];

@NgModule({
    imports: [
      CommonModule,
      BrowserModule,
      RouterModule.forRoot(appRoutes)
    ],
    exports: [
    ],
  })
  export class AppRoutingModule { }
