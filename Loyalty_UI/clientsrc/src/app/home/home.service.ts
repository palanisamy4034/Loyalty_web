import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Rx';
//import 'rxjs/add/operator/toPromise';
import './../../../../node_modules/rxjs/add/operator/map';
import './../../../../node_modules/rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import {} from './home.model';


@Injectable()
export class LoyaltyService {

    private TestUrl = "http://jsonplaceholder.typicode.com/posts";

    public headers: Headers;
    constructor(private http: Http) {
        this.headers = new Headers();
        this.headers.append('Access-Control-Allow-Headers', 'Content-Type');
        this.headers.append('Access-Control-Allow-Methods', 'GET, POST');
        this.headers.append('Access-Control-Allow-Origin', '*');
        this.headers.append('Content-Type', 'application/x-www-form-urlencoded');
    }


    private handleError(error: any) {
        let errMsg = (error.message) ? error.message :
            error.status ? `${error.status} - ${error.statusText}` : 'Server error';
        console.error(errMsg);
        return Observable.throw(errMsg);
    }

    private extractData(res: Response) {
        let body = res.json()
        return body || {};
    }

    private extractedData(res: Response) {
        let body = res.json()
        return JSON.stringify(body || {});
    }

    
      
}
