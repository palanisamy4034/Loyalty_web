import { Component, NgModule, Input, OnInit, trigger, state, style, animate, transition, ViewContainerRef, ElementRef, HostListener } from '../../../../node_modules/@angular/core';
import { LoyaltyService } from './home.service';
import { Router, ActivatedRoute, Params } from '../../../../node_modules/@angular/router';
//import { posts } from './home.model';
import { MdDatepickerInputEvent, MdButtonModule, MdButton } from '../../../../node_modules/@angular/material'
import { FormBuilder, FormGroup, Validators, FormArray, FormControl } from '@angular/forms';
import { } from '../../../../node_modules/@angular/forms';
import { itemValues } from './home.model';

import { BrowserModule } from '../../../../node_modules/@angular/platform-browser'

@Component({
  selector: 'Loyalty',
  templateUrl: 'home.component.html',
  styleUrls: ['home.component.css'],
  providers: [LoyaltyService]
})
export class LoyaltyComponent implements OnInit {
  startDate: Date;
  endDate: Date;
  touch: boolean;
  datepickerDisabled: boolean; name: string;
  //searchForm: FormGroup;
  public slabForm: FormGroup;
  _itemValues: FormGroup;
  public LoyaltyPrgm:FormGroup;
  public inputGroupVales: {};
  public itemRowCount: any;
  _true: boolean = false;

  

  @HostListener('body:click', ['$event'])
  clickout(event: any): void {
    if (this.eRef.nativeElement.contains(event.target)) {
      if (event.target.type == 'number' || event.target.type == 'text') {
        let ele = event.target;
        ele.removeAttribute("style");
      }
    } else {
      //this.text = "clicked outside";
    }
  }

  constructor(private _fb: FormBuilder,private _fb1: FormBuilder, private eRef: ElementRef) {
    this.slabForm = this._fb.group({
      baseAmount: [''], endAmount: [''], allocatedPoints: ['']
    });
  }

  ngOnInit() {
    this.slabForm = this._fb.group({
      itemRows: this._fb.array([this.initItemRows()])
    });
    this.LoyaltyPrgm = this._fb.group({
      prgmName:[''],
      prgmDescription: [''],
      prgmStartDate: [],
      prgmEndDate: []
  
    });
    console.log(this.LoyaltyPrgm);
  }
  initItemRows() {

    let _itemRowValue = this.slabForm.value.itemRows;//.itemRows;
    let _initialValue = 1;
    if (_itemRowValue != undefined) {
      let _rowCount = _itemRowValue.length - 1;
      _initialValue = _rowCount >= 0 ? _itemRowValue[_rowCount]['endAmount'] : 0;
      _initialValue = Number(_initialValue) + 1;
    }
    // _initialValue=Number(_initialValue);
    return this._fb.group({
      baseAmount: [_initialValue], endAmount: [''], allocatedPoints: ['']
    });
  }

  public addNewRow() {
    let verify = this.loyaltyPointsValidation();
    if (verify) {
      const control = <FormArray>this.slabForm.controls['itemRows'];
      control.push(this.initItemRows());
    }
  }

  deleteRow(index: number) {
    const control = <FormArray>this.slabForm.controls['itemRows'];
    control.removeAt(index);
  }

  public loyaltyPointsValidation() {
    let _itemRowValue = this.slabForm.value.itemRows;//.itemRows;
    if (_itemRowValue != undefined) {
      let _rowCount = _itemRowValue.length - 1;
      let _previousRow = _itemRowValue.length - 2;
      if (_itemRowValue[_rowCount]['baseAmount'] != '' && _itemRowValue[_rowCount]['endAmount'] != '' && _itemRowValue[_rowCount]['allocatedPoints'] != '') {
        if (_itemRowValue[_rowCount]['baseAmount'] < _itemRowValue[_rowCount]['endAmount'] && _rowCount > 0) {
          if (_itemRowValue[_rowCount]['baseAmount'] > _itemRowValue[_previousRow]['endAmount']) {
            console.log('dds', _itemRowValue[_rowCount]['baseAmount'])
            return true
          } else {
            var d = document.getElementById("baseAmount_" + _rowCount).style.border = '1px solid #a94442';
            return false;
          }
        }
        else if (_itemRowValue[_rowCount]['baseAmount'] > _itemRowValue[_rowCount]['endAmount']) {
          var d = document.getElementById("endAmount_" + _rowCount).style.border = '1px solid #a94442';
          return false;
        } else if (_itemRowValue[_rowCount]['baseAmount'] >= 0 && _itemRowValue[_rowCount]['baseAmount'] < _itemRowValue[_rowCount]['endAmount']) {
          return true;
        } else {
          var d = document.getElementById("endAmount_" + _rowCount).style.border = '1px solid #a94442';
          return false;
        }
      } else if (_itemRowValue[_rowCount]['baseAmount'] == '') {
        var d = document.getElementById("baseAmount_" + _rowCount).style.border = '1px solid #a94442';
        return false;
      } else if (_itemRowValue[_rowCount]['endAmount'] == '') {
        var d = document.getElementById("endAmount_" + _rowCount).style.border = '1px solid #a94442';
        return false;
      } else {
        var d = document.getElementById("allocatedPoints_" + _rowCount).style.border = '1px solid #a94442';
        return false;

      }
    }
    return true;

  }
  public login() {
    console.log(event);
    //console.log(this.loginForm.value);
  }

}