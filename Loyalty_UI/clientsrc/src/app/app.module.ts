/**core angular components */
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '../../../node_modules/@angular/core';
import { Component, Input, OnInit, trigger, state, style, animate, transition } from '../../../node_modules/@angular/core';
import { FormsModule,ReactiveFormsModule } from '../../../node_modules/@angular/forms'
import { BrowserModule } from '../../../node_modules/@angular/platform-browser';
import { HttpModule } from '../../../node_modules/@angular/http';
import { AppRoutingModule } from './app.routing'
import { RouterModule, Routes } from '../../../node_modules/@angular/router';
import { ComponentsModule } from './components/components.module';

// /*app component */
import { AppComponent } from './app.component';

// /* customer Home component */
import { LoyaltyService, LoyaltyComponent } from './home/home.index';

/**third party components */
import { SidebarModule } from '../../../node_modules/ng-sidebar';
import { BrowserAnimationsModule } from '../../../node_modules/@angular/platform-browser/animations'
import { MdDatepickerModule, MdNativeDateModule, MdInputModule, MdSidenavModule,MdButtonModule } from '../../../node_modules/@angular/material';

import 'hammerjs';
@NgModule({
    imports:
    [
        BrowserModule,FormsModule,  ReactiveFormsModule, BrowserAnimationsModule, MdSidenavModule, AppRoutingModule, RouterModule,
        HttpModule, MdDatepickerModule,MdNativeDateModule, MdInputModule,MdButtonModule, ComponentsModule],
    declarations: [AppComponent, LoyaltyComponent],
    bootstrap: [AppComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA],
    exports:[
        MdDatepickerModule,MdButtonModule]
})
export class AppModule { }